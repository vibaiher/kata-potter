class Price
  BOOK_PRICE = 8
  TWO_BOOKS_DISCOUNT = 0.05

  class << self
    def of(books)
      price = sum_individual_prices_of(books)
      discount = calculate_discount_for(books)

      price - discount
    end

    private

    def sum_individual_prices_of(books)
      BOOK_PRICE * books.count
    end

    def calculate_discount_for(books)
      return 0 if books.count == 1 || books.count != books.uniq.count

      sum_individual_prices_of(books) * TWO_BOOKS_DISCOUNT
    end
  end
end
