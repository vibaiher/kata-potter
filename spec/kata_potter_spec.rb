require 'price'

RSpec.describe 'Kata Potter' do
  INDIVIDUAL_PRICE = 8
  TWO_BOOKS_DISCOUNT = 0.05

  it 'calculates books price' do
    individual_price = INDIVIDUAL_PRICE
    books = [:first_book]

    price = Price.of(books)

    expect(price).to eq(individual_price)
  end

  it 'sums all book prices when have duplicates of same book' do
    duplicated_price = INDIVIDUAL_PRICE * 2
    books = [:first_book, :first_book]

    price = Price.of(books)

    expect(price).to eq(duplicated_price)
  end

  it 'applies a discount when customer buys two different books of the serie' do
    discounted_price = INDIVIDUAL_PRICE * 2 - (INDIVIDUAL_PRICE * 2 * TWO_BOOKS_DISCOUNT)
    books = [:first_book, :second_book]

    price = Price.of(books)

    expect(price).to eq(discounted_price)
  end
end
